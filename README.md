

# **Termômetro**


## Descrição

O termômetro tem como função principal manter a ordem e o respeito mutuo. 

Para um melhor convivio, por favor leia e siga as regras do grupo.


<summary>Regras do termômetro:</summary>


1. será iniciado toda semana com o valor de 0;

2. o termômetro tem inicio imediato;

3. cada vez que eu precisar intervir, será acrescentado +10;

4. quando o termômetro chegar ao valor de 100, será aplicada a penalidade à TODA SALA;

5. penalidade atual 1 ponto;

6. se ao final da semana, incluindo sábados e domingos, o termômetro estiver com menos de 100, não acontecerá a penalidade.

7. não importa quem começou ou participou;

8. as regras são fluidas, podendo ser ampliadas ou até revogadas, dependendo do comportamento.


